<?php



function child_twentyten_widgets_init() {

	// Area 1, located at the top of the sidebar.

	register_sidebar( array(

		'name' => __( 'Home Sidebar Area', 'twentyten' ),

		'id' => 'primary-widget-area',

		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),

		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',

		'after_widget' => '</li>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	) );



	register_sidebar( array(

		'name' => __( 'Footer - Widget', 'twentyten' ),

		'id' => 'footer-widget-area',

		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),

		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',

		'after_widget' => '</li>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	) );



}

//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS

add_action('after_setup_theme','remove_parent_widgets');

function remove_parent_widgets() {

    remove_action( 'widgets_init', 'twentyten_widgets_init' );

}

//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION

add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );


// enqueue style and javascript
function esposito_style_scripts() {
    wp_enqueue_style('custom-styles', get_stylesheet_directory_uri() . '/custom-styles.css', array(), true);
}
add_action( 'wp_enqueue_scripts', 'esposito_style_scripts' );



?>