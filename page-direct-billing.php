<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main">
			<div class="direct-billing">
				<?php
				/* Run the loop to output the page.
				 * If you want to overload this in a child theme then include a file
				 * called loop-page.php and that will be used instead.
				 */
				get_template_part( 'loop', 'page' );
				?>
				</section>
				<section class="insurance-companies">
					<div class="insurance-companies-container">
						<?php if(have_rows("insurance_companies")) : ?>
							<?php while(have_rows("insurance_companies")): the_row(); ?>
								<?php 
									$company_name = get_sub_field("company_name");
									$company_logo = get_sub_field("company_logo");
								 ?>
								 <div class="insurance-company">
								 	<img src="<?php echo $company_logo['url']; ?>" alt="<?php echo $company_logo['alt']; ?>">
								 	<h3><?php echo $company_name; ?></h3>
								 </div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</section>
				<section class="ending-part">
					<?php 
						$ending_section_text = get_field("ending_section_text");
						$book_appointment = get_field("book_appointment");
						if($ending_section_text){
							echo "<div class='ending-part-text'>";
							echo $ending_section_text;
							echo "</div>";
						}
						if($book_appointment){ ?>
							<a class="booknow booknow-no-float" href="<?php echo esc_url($book_appointment['url']); ?>"><?php echo esc_html($book_appointment['title']); ?></a>
						<?php
							}
						 ?>
				</section>
				</section>
			</div>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
