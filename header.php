<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="google-site-verification" content="wVHSQrQwpYV36BNeUiv4-Yi4fGrCjf-iJb1IqceyyjM" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="profile" href="https://gmpg.org/xfn/11" />
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php //bloginfo( 'stylesheet_url' ); ?>" />-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
?>



<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style-0827153.css" media="screen and (min-device-width: 481px)" type="text/css" />
<link type="text/css" rel="stylesheet" media="only screen and (max-device-width: 480px)" href="<?php echo dirname(get_stylesheet_uri()); ?>/phone_style.css" />
<link type="text/css" rel="stylesheet" href="<?php echo dirname(get_stylesheet_uri()); ?>/grid.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<meta name="viewport" content="width=device-width, minimum-scale=1.0, 
 maximum-scale=1.0">
<?php
?>
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>"  media="screen" />
<![endif]-->

 <?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61007302-1', 'auto');
  ga('send', 'pageview');

</script>

<style>
.metaslider {
    clear: both;
}
.flex-control-nav {
    bottom: 15px !important;
    z-index: 99 !important;
}
.metaslider .flexslider{
    margin-bottom: 0 !important;
}
button#responsive-menu-button {
    width: 100% !important;
    z-index: 1;
}
</style>
</head>
<body <?php body_class(); ?>>

<div id="headwrap">
	<div id="header">

<a href="/"><img src="https://www.espositomassage.com/wp-content/uploads/2015/06/Esposito-Logo.jpg" class="espositoLogo" width="auto" height="auto" alt="Esposito Logo"></a>

<div class="phone">
<p class="callUs">Call Us <br/> <a href="tel:6049364132">(604) 936-4132</a></p>
</div>

<a href="https://www.espositomassage.com/book-online/" target="_blank" class="booknow">Book Online</a>

<?php 
	if (is_front_page()) { ?>
<?php
		//echo do_shortcode('[huge_it_slider id="1"]');
		echo do_shortcode("[metaslider id=877]");
   	}
?>

<?php echo do_shortcode('[responsive_menu]'); ?>
			<div id="access" role="navigation">
			  <?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
				<?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #access -->
	</div><!-- #header -->
</div><!-- #headwrap -->
<div id="wrapper" class="hfeed">

	<div id="main">
