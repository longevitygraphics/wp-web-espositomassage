<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

    </div><!-- #main -->

</div><!-- #wrapper -->
<div id="bar">
We bill directly to all major Insurance companies
</div>
    <div id="footer" role="contentinfo">
        <div class="footerContent">
            <div class="grid">
<div class="col-1of3">
    <div class="footer-text1">
            &copy; <?php echo the_time('Y'); ?> Elizabeth Esposito, RMT & Esposito Massage<br/>
            <a href="https://www.longevitygraphics.com" target="_blank">Website Design</a> by <a href="https://www.longevitygraphics.com" target="_blank">Longevity Graphics</a> | <a href="/privacy-policy">Privacy Policy</a>
    </div>
</div>

<div class="col-1of3 socialicons">
    <div class="footer-text2">
        <p>
            <a class='facebook-icon icon-link' href="https://www.facebook.com/EspositoMassageTherapy" target="_blank">
            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                <path fill="#fff" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path>
            </svg></a>

            <a class='twitter-icon icon-link' href="https://twitter.com/@espositomassage" target="_blank">
            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" class="svg-inline--fa fa-twitter fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path fill="#fff" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>
                </a>

                <a class='googe-icon icon-link' href="https://www.google.com/search?ei=g0pFX8jxCcT3-gSi_47IDA&q=esposito+massage&gs_ssp=eJzj4tZP1zcsSTMryjDNNWC0UjWoMDWxMDO3NDBKtEg2TbZMMrUyqEiyNDc3MEy0SDRNNDAwNTDwEkgtLsgvzizJV8hNLC5OTE8FAKoIFH8&oq=esposito&gs_lcp=CgZwc3ktYWIQAxgAMgsILhDHARCvARCTAjIFCAAQsQMyCAguEMcBEK8BMgIIADICCC4yAggAMgIILjICCAAyAggAMgIIADoFCAAQkQI6CAguELEDEIMBOggIABCxAxCDAToICC4QxwEQowI6CggAELEDEIMBEEM6BAgAEEM6BQguELEDOgcIABCxAxBDOgQILhBDOgQIABAKUIawnwJY7b6fAmDzzJ8CaABwAHgAgAHwAYgB1QiSAQUyLjQuMpgBAKABAaoBB2d3cy13aXrAAQE&sclient=psy-ab#lrd=0x54867902a8c5c9b5:0xb97701a8a5a00500,1,,," target="_blank">
                    <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="google" class="svg-inline--fa fa-google fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 512">
                        <path fill="#fff" d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"></path>
                    </svg>
                </a>

                <a class='instagram-icon icon-link' href="https://www.instagram.com/espositomassage/" target="_blank">
                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="#fff" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                </svg>
                </a>

                <a class='pinterest-icon icon-link' href="https://id.pinterest.com/pin/229402174752344272/" target="_blank">
                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="pinterest-p" class="svg-inline--fa fa-pinterest-p fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                <path fill="#fff" d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z"></path></svg>
                </a>
            </p>
    </div>
</div>

<div class="col-1of3">
<div class="footer-text3">
    <div class="address">
        <div itemscope itemtype="http://schema.org/LocalBusiness">
        <span itemprop="name">Esposito Massage</span><br/>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">2227 St. Johns Street,</span><br/>
        <span itemprop="addressLocality">Port Moody</span>, <span itemprop="addressRegion">BC</span> <span itemprop="postalCode">V3H 2A6</span><br/>
    </div>
    <span itemprop="telephone">(604) 936-4132</span>
</div> <!-- .footer-text3 -->
</div> <!-- .col-1of3 -->

</div>

</div>
</div>
<div id="popup-rating-widget"><script id="popup-rating-widget-script" src="https://widget.reviewability.com/js/popupWidget.min.js" data-gfspw="https://app.gatherup.com/popup-pixel/get/fe57e716da9544656e5a013b0776f8cfa6065bd2" async></script></div>


<?php
    /* A sidebar in the footer? Yep. You can can customize
     * your footer with four columns of widgets.
     */
    get_sidebar( 'footer' );
?>
    </div><!-- #footer -->
<?php
    /* Always have wp_footer() just before the closing </body>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to reference JavaScript files.
     */

    wp_footer();
?>
</body>
</html>